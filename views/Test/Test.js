QUnit.test('Testing the BMI calculator with four sets of inputs', function (assert) {
    assert.equal(CalculateBMI(12, 2), 3.00, "works with two positive integers");
    assert.equal(CalculateBMI(-2, 12), -0.01, "works with a positive and a negative number");
    assert.equal(CalculateBMI(-2, -12), -0.01, "works with two negative integers");
    assert.equal(CalculateBMI(0, 0), "NaN", "works with zeroes");
});

QUnit.test('Testing the BMI calculator with four sets of inputs to check exception handling', function(assert) {
    assert.throws(function() { CalculateBMI(NaN, NaN) }, 'NaN is restricted');
    assert.throws(function() { CalculateBMI(null, null) }, 'Null is restricted');
    assert.throws(function() { CalculateBMI("abc", "def") }, 'String is restricted');
    assert.throws(function() { CalculateBMI(undefined, undefined) }, 'Undefined values are restricted');
});